# import sys
# reload(sys)
# sys.setdefaultencoding('utf-8')

# Python Data Analysis Library https://pandas.pydata.org
import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
# seaborn: statistical data visualization https://seaborn.pydata.org
# import seaborn as sns

# import warnings
# warnings.filterwarnings('ignore')

def main():
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html#pandas.read_csv
    dataFrame = pd.read_csv("./diabetes/diabetes.csv")

    # print(dataFrame.head())

    # df = pd.DataFrame({
    #     'length': [1.5, 0.5, 1.2, 0.9, 3],
    #     'width': [0.7, 0.2, 0.15, 0.2, 1.1]
    #     }, index= ['pig', 'rabbit', 'duck', 'chicken', 'horse'])

    # hist = df.hist(bins=3)

    # dataFrame.head()
    # print(dataFrame.shape)
    print(dataFrame.describe())

    # A histogram is a representation of the distribution of data
    # one diagram per each column
    # dataFrame.hist(figsize=(16,14))

    # Draw one histogram of the DataFrame’s columns.
    # This is useful when the DataFrame’s Series are in a similar scale.
    # That is why we need to do normalization
    # ax = dataFrame.plot.hist(alpha=0.5, bins=1000)
    # save to file
    # fig = ax.get_figure()
    # fig.savefig('test.png')

    # sns.pairplot(dataFrame)
    # dataFrame.plot(kind="box")

main()

