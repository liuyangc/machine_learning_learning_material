import math
import operator 

# normalization
def normalization(dataset):

    # skip label, loop each column
    for col in range(0, len(dataset[0]) - 1):
        minVal = math.inf
        maxVal = -math.inf

        # skip title, loop each row
        for row in range(1, len(dataset)):
            minVal = min(dataset[row][col], minVal)
            maxVal = max(dataset[row][col], maxVal)

        for row in range(1, len(dataset)):
            dataset[row][col] = (dataset[row][col] - minVal) / (maxVal - minVal)

    return dataset

# calculate metric
def euclideanDistance(instance1, instance2, length):
  distance = 0
  
  for x in range(length):
    distance += pow((instance1[x] - instance2[x]), 2)

  return math.sqrt(distance)

# get k nearest neighbors
def getNeighbors(trainingSet, testInstance, k):
  distances = []
  length = len(testInstance)-1
  
  for x in range(len(trainingSet)):
    dist = euclideanDistance(testInstance, trainingSet[x], length)
    # remember corresponding training instance
    distances.append((trainingSet[x], dist))

  distances.sort(key=operator.itemgetter(1))

  neighbors = []
  for x in range(k):
    neighbors.append(distances[x][0])
  
  return neighbors

# voting process, get the most voted result from k neighbors
def getDecision(neighbors):
  classVotes = {}
  
  for x in range(len(neighbors)):
    # get labels of known instances
    decision = neighbors[x][-1]

    if decision in classVotes:
      classVotes[decision] += 1
    else:
      classVotes[decision] = 1

  # get most voted decision
  # As you are in python3 , use dict.items() instead of dict.iteritems()
  sortedVotes = sorted(classVotes.items(), key=operator.itemgetter(1), reverse=True)
  
  return sortedVotes[0][0]


# accuracy, (true postive + true negative) / total testSet
def getAccuracy(testSet, predictions):
  correct = 0
  
  truePositive = 0
  trueNegative = 0
  falsePositive = 0
  falseNegative = 0

  for x in range(len(testSet)):
    if testSet[x][-1] is predictions[x]:
        if testSet[x][-1] is '1':
            truePositive += 1
        else:
            trueNegative += 1
        correct += 1
    else:
        if testSet[x][-1] is '1':
            falsePositive += 1
        else:
            falseNegative += 1
    

  print('truePositive: ', truePositive, 'trueNegative: ', trueNegative, 'falsePositive: ', falsePositive, 'falseNegative: ', falseNegative)
  print()



  return (correct/float(len(testSet))) * 100.0
