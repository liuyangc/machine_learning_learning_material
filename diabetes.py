from read_file import load
from utils import euclideanDistance, getNeighbors, getDecision, getAccuracy
import time

def main():
    trainingSet=[]
    testSet=[]
    split = 0.8
    
    # load data
    load('diabetes/diabetes.csv', split, trainingSet, testSet)

    start = time.clock()
    
    # print 'Train set: ' + repr(len(trainingSet))
    # print 'Test set: ' + repr(len(testSet))
	
    # generated predictions to the testSet
    predictions=[]

    # k=3, accuracy = 62.82051282051282%
    k = 19

    for x in range(len(testSet)):
      neighbors = getNeighbors(trainingSet, testSet[x], k)
    #   print(neighbors)
    #   exit()
      result = getDecision(neighbors)
      predictions.append(result)
    #   print('> predicted=' + repr(result) + ', actual=' + repr(testSet[x][-1]))

    end = time.clock()
    accuracy = getAccuracy(testSet, predictions)
    
    print('time: ', end - start)
    print('Accuracy: ' + repr(accuracy) + '%')

main()
