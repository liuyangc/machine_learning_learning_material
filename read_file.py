import csv
import random
from utils import normalization

def load(filename, split, trainingSet=[] , testSet=[]):
    with open(filename, 'rt') as csvfile:
        lines = csv.reader(csvfile)
        dataset = list(lines)
        
        for x in range(1, len(dataset)):
            # inlcude label right at the last element
            for y in range(len(dataset[0])-1):
                dataset[x][y] = float(dataset[x][y])

        dataset = normalization(dataset)
        # print(dataset)

        for x in range(1, len(dataset)-1):
            # {split}% of dataset comes to trainingSet, just randomly pick
            if x < split * len(dataset):
                trainingSet.append(dataset[x])
            else:
                testSet.append(dataset[x])
        